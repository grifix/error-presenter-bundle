<?php

declare(strict_types=1);

namespace Grifix\ErrorPresenterBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

final class Configuration implements ConfigurationInterface
{

    public function getConfigTreeBuilder(): TreeBuilder
    {
        $treeBuilder = new TreeBuilder('grifix_error_presenter');

        /** @formatter:off */
        $treeBuilder->getRootNode()
            ->children()
                ->booleanNode('debug_exceptions')
                ->end()
            ->end();

        /** @formatter:on */
        return $treeBuilder;
    }
}
