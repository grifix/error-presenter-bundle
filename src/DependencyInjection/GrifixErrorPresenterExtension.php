<?php

declare(strict_types=1);

namespace Grifix\ErrorPresenterBundle\DependencyInjection;

use Grifix\ErrorPresenterBundle\ExceptionConverterProviderInterface;
use Grifix\ErrorPresenterBundle\GrifixErrorPresenterBundle;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;
use Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

final class GrifixErrorPresenterExtension extends Extension implements PrependExtensionInterface
{

    public function prepend(ContainerBuilder $container)
    {
        $this->checkBundles($container);
        $this->loadConfigs($container);
    }

    private function checkBundles(ContainerBuilder $container): void
    {
        $bundles = $container->getParameter('kernel.bundles');
        if (!isset($bundles['FrameworkBundle'])) {
            throw new RuntimeException('FrameworkBundle must be enabled!');
        }
    }

    private function loadConfigs(ContainerBuilder $container): void
    {
        $configDir = __DIR__ . '/../..';
        $loader = new YamlFileLoader($container, new FileLocator($configDir));
        $loader->load('config.yaml');
    }


    public function load(array $configs, ContainerBuilder $container)
    {
        $this->loadBundleConfig($configs, $container);
        $container
            ->registerForAutoconfiguration(ExceptionConverterProviderInterface::class)
            ->addTag(GrifixErrorPresenterBundle::PROVIDER_TAG);
    }

    private function loadBundleConfig(array $configs, ContainerBuilder $container): void
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);
        foreach ($config as $key => $value) {
            $container->setParameter(GrifixErrorPresenterBundle::PREFIX . '.' . $key, $value);
        }
    }

}
