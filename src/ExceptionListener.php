<?php

declare(strict_types=1);

namespace Grifix\ErrorPresenterBundle;

use Grifix\ErrorPresenter\ErrorPresenter;
use Grifix\ErrorPresenter\ExceptionConverterInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\Messenger\Exception\HandlerFailedException;

final class ExceptionListener
{
    /** @var ExceptionConverterInterface[] */
    private array $converters = [];

    public function __construct(readonly bool $debugExceptions = false)
    {
    }

    public function onException(ExceptionEvent $event): void
    {
        $exception = $event->getThrowable();
        if ($exception instanceof HandlerFailedException) {
            $exception = $exception->getPrevious();
        }
        if ($this->debugExceptions) {
            $event->stopPropagation();
            throw $exception;
        }
        $presenter = new ErrorPresenter(...$this->converters);
        $error = $presenter->present($exception);
        $event->setResponse(
            new JsonResponse(
                $error->content,
                $error->httpCode,
            )
        );
    }

    public function addExceptionConverterProvider(ExceptionConverterProviderInterface $exceptionConverterProvider): void
    {
        $this->converters = array_merge($this->converters, $exceptionConverterProvider->getConverters());
    }
}
