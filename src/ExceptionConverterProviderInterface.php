<?php

declare(strict_types=1);

namespace Grifix\ErrorPresenterBundle;

use Grifix\ErrorPresenter\ExceptionConverterInterface;

interface ExceptionConverterProviderInterface
{

    /**
     * @return ExceptionConverterInterface[]
     */
    public function getConverters(): array;
}
