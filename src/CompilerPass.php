<?php

declare(strict_types=1);

namespace Grifix\ErrorPresenterBundle;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

final class CompilerPass implements CompilerPassInterface
{

    public function process(ContainerBuilder $container)
    {
        $definition = $container->findDefinition(ExceptionListener::class);
        $providers = $container->findTaggedServiceIds(GrifixErrorPresenterBundle::PROVIDER_TAG);

        foreach ($providers as $id => $tags) {
            $definition->addMethodCall('addExceptionConverterProvider', [new Reference($id)]);
        }
    }
}
