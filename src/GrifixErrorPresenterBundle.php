<?php
declare(strict_types=1);

namespace Grifix\ErrorPresenterBundle;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

final class GrifixErrorPresenterBundle extends Bundle
{
    public const PREFIX = 'grifix_error_presenter';
    public const PROVIDER_TAG = 'grifix.error_presenter_exception_converter_provider';



    public function build(ContainerBuilder $container)
    {
        parent::build($container);
        $container->addCompilerPass(new CompilerPass());
    }
}

