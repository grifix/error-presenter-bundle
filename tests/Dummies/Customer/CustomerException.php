<?php

declare(strict_types=1);

namespace Grifix\ErrorPresenterBundle\Tests\Dummies\Customer;

final class CustomerException extends \Exception
{
    protected $message = 'Customer exception!';
}
