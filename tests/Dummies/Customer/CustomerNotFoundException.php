<?php

declare(strict_types=1);

namespace Grifix\ErrorPresenterBundle\Tests\Dummies\Customer;

final class CustomerNotFoundException extends \Exception
{
    protected $message = 'Customer not found!';
}
