<?php

declare(strict_types=1);

namespace Grifix\ErrorPresenterBundle\Tests\Dummies\Customer;


use Grifix\ErrorPresenter\ExceptionConverter;
use Grifix\ErrorPresenterBundle\ExceptionConverterProviderInterface;

final class CustomerExceptionConverterProvider implements ExceptionConverterProviderInterface
{
    public function getConverters(): array
    {
        return [
            ExceptionConverter::create(CustomerNotFoundException::class, 504),
            ExceptionConverter::create('Grifix\ErrorPresenterBundle\Tests\Dummies\Customer', 500),
        ];
    }
}
