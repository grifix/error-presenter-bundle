<?php

declare(strict_types=1);

namespace Grifix\ErrorPresenterBundle\Tests\Dummies;

use Grifix\ErrorPresenterBundle\Tests\Dummies\Customer\CustomerException;
use Grifix\ErrorPresenterBundle\Tests\Dummies\Customer\CustomerNotFoundException;
use Grifix\ErrorPresenterBundle\Tests\Dummies\User\UserException;
use Grifix\ErrorPresenterBundle\Tests\Dummies\User\UserNotFoundException;
use Symfony\Component\Routing\Annotation\Route;

final class RequestHandler
{
    #[Route('/test/{action}')]
    public function __invoke(string $action)
    {
        match ($action) {
            'findUser' => throw new UserNotFoundException(),
            'user' => throw new UserException(),
            'customer' => throw new CustomerException(),
            'findCustomer' => throw new CustomerNotFoundException(),
        };
    }
}
