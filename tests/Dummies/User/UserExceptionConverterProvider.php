<?php

declare(strict_types=1);

namespace Grifix\ErrorPresenterBundle\Tests\Dummies\User;


use Grifix\ErrorPresenter\ExceptionConverter;
use Grifix\ErrorPresenterBundle\ExceptionConverterProviderInterface;

final class UserExceptionConverterProvider implements ExceptionConverterProviderInterface
{

    protected function getStatusCodeMap(): array
    {
        return [
            'Grifix\ErrorPresenterBundle\Tests\Dummies\User' => 400,
            UserNotFoundException::class => 404
        ];
    }

    public function getConverters(): array
    {
        return [
            ExceptionConverter::create(UserNotFoundException::class, 404),
            ExceptionConverter::create('Grifix\ErrorPresenterBundle\Tests\Dummies\User', 400)
        ];
    }
}
