<?php

declare(strict_types=1);

namespace Grifix\ErrorPresenterBundle\Tests\Dummies\User;

final class UserNotFoundException extends \Exception
{
    protected $message = 'User not found!';
}
