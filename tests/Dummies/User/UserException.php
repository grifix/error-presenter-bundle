<?php

declare(strict_types=1);

namespace Grifix\ErrorPresenterBundle\Tests\Dummies\User;

final class UserException extends \Exception
{
    protected $message = 'User exception!';
}
