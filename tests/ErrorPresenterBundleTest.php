<?php

declare(strict_types=1);

namespace Grifix\ErrorPresenterBundle\Tests;

use Grifix\ErrorPresenterBundle\GrifixErrorPresenterBundle;
use Grifix\ErrorPresenterBundle\Tests\Dummies\User\UserNotFoundException;
use Nyholm\BundleTest\TestKernel;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\KernelInterface;

final class ErrorPresenterBundleTest extends KernelTestCase
{
    protected static function getKernelClass(): string
    {
        return TestKernel::class;
    }

    protected static function createKernel(array $options = []): KernelInterface
    {
        /**
         * @var TestKernel $kernel
         */
        $kernel = parent::createKernel($options);
        $kernel->addTestBundle(GrifixErrorPresenterBundle::class);
        $kernel->addTestConfig(__DIR__ . '/test_config.yaml');
        $kernel->addTestRoutingFile(__DIR__ . '/test_routes.yaml');
        $kernel->handleOptions($options);

        return $kernel;
    }

    /**
     * @dataProvider itHandlesDataProvider
     */
    public function testItHandles(string $action, string $expectedMessage, int $expectedStatusCode): void
    {
        putenv("DEBUG_EXCEPTIONS=false");
        $kernel = self::bootKernel();
        $response = $kernel->handle(Request::create('/test/' . $action));
        self::assertInstanceOf(JsonResponse::class, $response);
        self::assertEquals($expectedStatusCode, $response->getStatusCode());
        self::assertEquals(
            [
                'error' => $expectedMessage,
                'code' => 0,
            ],
            json_decode($response->getContent(), true)
        );
    }

    public function itHandlesDataProvider(): array
    {
        return [
            'user' => [
                'user',
                'User exception!',
                400
            ],
            'user not found' => [
                'findUser',
                'User not found!',
                404
            ],
            'customer' => [
                'customer',
                'Customer exception!',
                500
            ],
            'customer not found' => [
                'findCustomer',
                'Customer not found!',
                504
            ],
        ];
    }

    public function testItDoesNotHandle():void{
        putenv("DEBUG_EXCEPTIONS=true");
        $kernel = self::bootKernel();
        self::expectException(UserNotFoundException::class);
        $kernel->handle(Request::create('/test/findUser'));
    }
}
